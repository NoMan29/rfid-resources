# RFID Resources

A list of resources for RFID. Those resources and tools are intendet only for penetration testers, red teamers and educational use in conrolled environment.

**What is [RFID](https://en.wikipedia.org/wiki/Radio-frequency_identification)?**

Radio-frequency identification (RFID) uses electromagnetic fields to automatically identify and track tags attached to objects. An RFID system consists of a tiny radio transponder, a radio receiver and transmitter. When triggered by an electromagnetic interrogation pulse from a nearby RFID reader device, the tag transmits digital data, usually an identifying inventory number, back to the reader. This number can be used to track inventory goods.
__________________________________

Table of content
----------------
1. Resources for RFID 
2. Contribution 

Resources for RFID 
======================

- http://www.proxmark.org/files/ - link provided by the  @herrmann1001 in twitter. 

Contribution
============
Every contribution is welcome! If you need more info on something check our website (https://abstract-security.lugons.org/), our Discord (https://discord.gg/zn3wsrr) or contact me (noman29@lugons.org).
